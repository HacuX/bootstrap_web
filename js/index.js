$(function () {
                $('[data-toggle="tooltip"]').tooltip();
                $('[data-toggle="popover"]').popover();
                $('.carousel').carousel({
                interval: 2000
                });

                $('#exampleModal').on('show.bs.modal', function (e) {
                    console.log('se inicia el modal');
                    $('#btnexampleModal').removeClass('btn-outline-secondary');
                    $('#btnexampleModal').addClass('btn-success');
                    $('#btnexampleModal').prop('disabled', true);
                });
                $('#exampleModal').on('shown.bs.modal', function (e) {
                    console.log('se muestra el modal');
                });
                $('#exampleModal').on('hide.bs.modal', function (e) {
                    console.log('el modal se cierra');
                });
                $('#exampleModal').on('hidden.bs.modal', function (e) {
                    console.log('el modal se oculto');
                    $('#btnexampleModal').removeClass('btn-success');
                    $('#btnexampleModal').addClass('btn-outline-secondary'
                    );                
                });
            });